<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //
    protected $fillable = ['name', 'surname', 'email', 'external_url', 'promo_id'];

    public function promo()
    {
        return $this->belongsTo(Promo::class);
    }
}
