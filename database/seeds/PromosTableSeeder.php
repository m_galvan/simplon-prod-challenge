<?php

use Illuminate\Database\Seeder;

class PromosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('promos')->insert([       // New promo
            'title' => 'Promo 9',
            'start' => date('2018-12-17'),
            'end' => date('2019-06-11'),            
        ]);
    }
}
